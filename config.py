# 基本配置
class Config(object):
    # 配置通用密钥
    SECRET_KEY = 'msqaidongyuqing'

    # 配置Mysql连接
    SQLALCHEMY_DATABASE_URI = 'mysql://root:123456@localhost:3306/jiayuan'
    SQLALCHEMY_TRACK_MODIFICATIONS = True


class DevelopmentConfig(Config):
    DEBUG=True

class ProductConfig(Config):
    pass


config_map = {
    'develop':DevelopmentConfig,
    'product':ProductConfig
}