from jiayuan import db


class EducationLevelCount(db.Model):
    __tablename__ = 'db_edu_level_count'
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    edu_level = db.Column(db.String(20),unique=True)
    count = db.Column(db.Integer)

class AreaCount(db.Model):
    __tablename__ = 'db_area_count'
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    area = db.Column(db.String(20),unique=True)
    count = db.Column(db.Integer)

class HeightAreaCount(db.Model):
    __tablename__ = 'db_height_area_count'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    height_area = db.Column(db.String(20),unique=True)
    count = db.Column(db.Integer)

class AgeAreaCount(db.Model):
    __tablename__ = 'db_age_area_count'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    age_area = db.Column(db.String(20), unique=True)
    count = db.Column(db.Integer)

