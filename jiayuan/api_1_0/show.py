from . import api
from jiayuan.models import AreaCount,EducationLevelCount,AgeAreaCount,HeightAreaCount
from flask import render_template

# 绘制不同地区的女性数量
@api.route('/drawBar')
def drawBar():
    area_count = AreaCount.query.all()
    area = [i.area for i in area_count]
    count = [i.count for i in area_count]
    return render_template('drawBar.html', **locals())

# 绘制不同学历的女性数量
@api.route('/drawPie')
def drawPie():
    height_area_count = HeightAreaCount.query.all()
    height_area_count_data = [{'name':i.height_area,'value':i.count} for i in height_area_count]

    age_area_count = AgeAreaCount.query.all()
    age_area_count_data = [{'name':i.age_area,'value':i.count} for i in age_area_count]

    return render_template('drawPie.html',**locals())

# 绘制不同身高区间的女性数量&不同年龄区间的女性数量
@api.route('/drawSignlePie')
def drawSignlePie():
    edu_level_count  = EducationLevelCount.query.all()
    edu_level_count_data = [{'name':i.edu_level,'value':i.count} for i in edu_level_count]
    return render_template('drawSignalPie.html',**locals())

# 展示词云图
@api.route('/drawWordCloud')
def drawWordCloud():
    return render_template('wordCloud.html')