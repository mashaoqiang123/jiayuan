from flask import Flask
from config import config_map
from flask_sqlalchemy import SQLAlchemy
import pymysql

pymysql.install_as_MySQLdb()
db = SQLAlchemy()

# 通过工厂模式来创建实例化app对象
def create_app(config_name='develop'):
    app = Flask(__name__)
    config  = config_map[config_name]
    app.config.from_object(config)

    # 初始化数据库
    db.init_app(app)

    # 注册蓝图
    from .api_1_0 import show
    app.register_blueprint(show.api,url_prefix='/show')

    return app
